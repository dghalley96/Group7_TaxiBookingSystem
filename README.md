## Getting Started

Welcome to the Taxi Booking System. The Taxi Booking System is a software solution designed to facilitate efficient booking and management of taxi services. It provides a platform for customers to book rides, drivers to accept and reject the booking, and administrators to oversee the entire operation. The system is built using various design patterns to ensure scalability, maintainability, and extensibility.

## Folder Structure

The workspace contains two folders by default, where:

- `src`: the folder contains the all the design pattern implementation for the project and graphical user interface.
- `lib`: the folder contains the jar files for the taxi booking and sql-connector for connection with mysql


## Roles and Responsibility
Deepak Ghalley - Project Lead
Chimi Rinzin - Project Manager
Dawa - Backend Developer
Thukden Dema - Backend Developer
>

## Dependency Management

The `JAVA PROJECTS` view allows you to manage your dependencies. More details can be found [here](https://github.com/microsoft/vscode-java-dependency#manage-dependencies).
